//Ejemplo 1 : Api básica

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());

app.get('/', function(req,res) {
    res.send("API de Catálogo Productos");
});

app.get('/producto', function(req,res) {
    var respuesta = {id : 1, nombre : 'Laptop', precio : 1500, stock : 5};
    res.send(respuesta);
});

app.post('/producto', function(req,res) {
    respuesta = {mensaje : 'producto creado'};
    console.log(req.body);
    if(!req.body.id || !req.body.nombre) {
        respuesta = {mensaje : 'error, se requiere el id y el nombre'};
    }
    console.log(req.body);
    res.send(respuesta);
});

app.put('/producto',function(req,res) {
    respuesta = {mensaje : 'producto modificado'};
    console.log(req.body);
    res.send(respuesta);
});

app.listen(3300, () => {
    console.log("El servidor está inicializado en el puerto 3300");
});


