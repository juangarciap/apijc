const userRoutes = (app, fs) => {

    // variables
    const dataPath = './data/users.json';

    // metodos de utilidad
    const readFile = (callback, returnJson = false, filePath = dataPath, encoding = 'utf8') => {
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                throw err;
            }

            callback(returnJson ? JSON.parse(data) : data);
        });
    };

    const writeFile = (fileData, callback, filePath = dataPath, encoding = 'utf8') => {

        fs.writeFile(filePath, fileData, encoding, (err) => {
            if (err) {
                throw err;
            }

            callback();
        });
    };

    // READ
    app.get('/users', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }

            res.send(JSON.parse(data));
        });
    });

    app.get('/users/:id', (req, res) => {
        readFile(data =>  {
            const userID = req.params["id"];
            const user = data[userID];
            res.send(user);
        },true);
    });

    // INSERT
    app.post('/users',(req,res) => {
        readFile(data =>  {
            const newID = Object.keys(data).length + 1;

            data[newID.toString()] = req.body;

            writeFile(JSON.stringify(data, null, 2), () => {
                res.status(200).send('Nuevo usuario grabado!');
            });
        
        }, true);
    });

    // MODIFICAR 
    app.put('/users/:id', (req,res) => {
        readFile (data => {
            const userID = req.params["id"];
            data[userID] = req.body;

            writeFile(JSON.stringify(data, null, 2), () => {
                res.status(200).send(`Usuario id:${userID} actualizado!`);
            });            
        }, true);
    });
    
    // DELETE
    app.delete('/users/:id', (req,res) => {
        readFile (data => {
            const userID = req.params["id"];
            delete data[userID];

            writeFile(JSON.stringify(data, null, 2), () => {
                res.status(200).send(`Usuario id:${userID} eliminado!`);
            });            
        }, true);
    });
}


module.exports = userRoutes;