const userRoutes =  require('./users');

const appRouter = (app, fs) => {

    //ruta default
    app.get('/', (req, res) => {
        res.send('Bienvenidos a mi API');
    });

    //otra rutas
    userRoutes(app, fs);

};

module.exports = appRouter;