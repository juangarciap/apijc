const userRoutesFB = (app, db) => {
    app.get('/usersfb', (req, res) => {
        db.collection('users').get()
            .then((snapshot) => {
                var docs = snapshot.docs.map(doc => doc.data());
                
                /*snapshot.forEach((doc) => {
                    console.log(doc.id,'-',doc.data());
                });*/

                res.send(docs);

            })
            .catch((err) => {
                console.log(err);
            });
    });

}


module.exports = userRoutesFB;