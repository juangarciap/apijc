const userRoutesFB =  require('./usersFB');

const appRouterFB = (app, db) => {

    //ruta default
    app.get('/', (req, res) => {
        res.send('Bienvenidos a mi API con Firebase');
    });

    //otra rutas
    userRoutesFB(app, db);

};

module.exports = appRouterFB;