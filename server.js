const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");

const admin = require('firebase-admin');
let serviceAccount = require('./keyfb.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
let db = admin.firestore();


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const routes = require('./routes/routes.js')(app, fs);
const routesFB = require('./routes/routesFB.js')(app, db);

const port = 1515;
app.listen(port, () => {
    console.log("El servidor está inicializado en el puerto ->" + port);
});


